package ch.hevs.populate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import ch.hevs.businessobject.Episode;
import ch.hevs.businessobject.Library;
import ch.hevs.businessobject.Season;
import ch.hevs.businessobject.Serie;
import ch.hevs.businessobject.User;
import ch.hevs.serieservice.TvSerieBean;
import ch.hevs.serieservice.TvSeries;

public class Friends {


	public static void  populateFriends(EntityManager em) {

		// SERIE
		Serie friends = new Serie();
		friends.setName("Friends");
		em.persist(friends);

		// SEASONS
		int friendsSeasonYear[] = { 1994, 1995, 1996, 1997 };
		List<Season> friendsSeasons = new ArrayList<Season>();

		for (int i = 0; i < friendsSeasonYear.length; i++) {
			int SeasonNumber = i + 1;
			Season s = new Season();
			s.setNumber(SeasonNumber);
			s.setYear(friendsSeasonYear[i]);
			friendsSeasons.add(s);
			friends.addSeason(friendsSeasons.get(i));
			em.persist(s);
		}

		// EPISODES
		Map<Integer, String> hm = new LinkedHashMap<>();
		hm.put(101, "Celui qui d�m�nage");
		hm.put(102, "Celui qui est perdu");
		hm.put(103, "Celui qui a un r�le");
		hm.put(104, "Celui avec George");
		hm.put(105, "Celui qui lave plus blanc");
		hm.put(106, "Celui qui est verni");
		hm.put(107, "Celui qui a du jus");
		hm.put(108, "Celui qui hallucine");
		hm.put(109, "Celui qui parle au ventre de sa femme");
		hm.put(110, "Celui qui singeait");
		hm.put(111, "Celui qui �tait comme les autres");
		hm.put(112, "Celui qui aimait les lasagnes");
		hm.put(113, "Celui qui fait des descentes dans les douches");
		hm.put(114, "Celui qui avait un c�ur d'artichaut");
		hm.put(115, "Celui qui p�te les plombs)");
		hm.put(116, "Celui qui devient papa ��1re�partie");
		hm.put(117, "Celui qui devient papa ��2e�partie");
		hm.put(118, "Celui qui gagnait au poker");
		hm.put(119, "Celui qui a perdu son singe");
		hm.put(120, "Celui qui a un dentiste cari�");
		hm.put(121, "Celui qui avait un singe");
		hm.put(122, "Celui qui r�ve par procuration");
		hm.put(123, "Celui qui a failli rater l'accouchement)");
		hm.put(124, "Celui qui fait craquer Rachel");
		hm.put(201, "Celui qui a une nouvelle fianc�e");
		hm.put(202, "Celui qui d�testait le lait maternel");
		hm.put(203, "Celui qui est mort dans l'appart du dessous");
		hm.put(204, "Celui qui avait vir� de bord");
		hm.put(205, "Celui qui se faisait passer pour Bob");
		hm.put(206, "Celui qui a oubli� un b�b� dans le bus");
		hm.put(207, "Celui qui tombe des nues");
		hm.put(208, "Celui qui a �t� tr�s maladroit");
		hm.put(209, "Celui qui cassait les radiateurs");
		hm.put(210, "Celui qui se d�double");
		hm.put(211, "Celui qui n'appr�cie pas certains mariages");
		hm.put(212, "Celui qui retrouve son singe -�1re�partie");
		hm.put(213, "Celui qui retrouve son singe -�2e�partie");
		hm.put(214, "Celui qui a failli aller au bal de promo");
		hm.put(215, "Celui qui a fait on ne sait quoi avec Rachel");
		hm.put(216, "Celui qui vit sa vie");
		hm.put(217, "Celui qui remplace celui qui part");
		hm.put(218, "Celui qui dispara�t de la s�rie");
		hm.put(219, "Celui qui ne voulait pas partir");
		hm.put(220, "Celui qui se met � parler");
		hm.put(221, "Celui qui affronte les voyous");
		hm.put(222, "Celui qui faisait le lien");
		hm.put(223, "Celui qui attrape la varicelle");
		hm.put(224, "Celui qui embrassait mal");
		hm.put(301, "Celui qui r�vait de la princesse Leia");
		hm.put(302, "Celui qui a du mal � se pr�parer");
		hm.put(303, "Celui qui avait la technique du c�lin");
		hm.put(304, "Celui qui ne supportait pas les poup�es");
		hm.put(305, "Celui qui bricolait");
		hm.put(306, "Celui qui se souvient");
		hm.put(307, "Celui qui �tait prof et �l�ve");
		hm.put(308, "Celui qui avait pris un coup sur la t�te");
		hm.put(309, "Celui pour qui le foot c'est pas le pied");
		hm.put(310, "Celui qui fait d�missionner Rachel");
		hm.put(311, "Celui qui ne s'y retrouvait plus");
		hm.put(312, "Celui qui �tait tr�s jaloux");
		hm.put(313, "Celui qui persiste et signe");
		hm.put(314, "Celui que les proth�ses ne g�naient pas");
		hm.put(315, "Celui qui vivait mal la rupture");
		hm.put(316, "Celui qui a surv�cu au lendemain");
		hm.put(317, "Celui qui �tait laiss� pour compte");
		hm.put(318, "Celui qui s'auto-hypnotisait");
		hm.put(319, "Celui qui avait un tee-shirt trop petit");
		hm.put(320, "Celui qui courait deux li�vres");
		hm.put(321, "Celui qui avait un poussin");
		hm.put(322, "Celui qui s'�nervait");
		hm.put(323, "Celui qui avait un truc dans le dos");
		hm.put(324, "Celui qui voulait �tre ultime champion");
		hm.put(325, "Celui qui allait � la plage");
		hm.put(401, "Celui qui soignait les piq�res de m�duses");
		hm.put(402, "Celui qui ne voyait qu'un chat");
		hm.put(403, "Celui qui avait des menottes");
		hm.put(404, "Celui qui apprenait � danser");
		hm.put(405, "Celui qui avait une nouvelle copine");
		hm.put(406, "Celui qui fr�quentait une souillon");
		hm.put(407, "Celui qui poussait le bouchon");
		hm.put(408, "Celui qui �tait dans la caisse");
		hm.put(409, "Celui qui savait faire la f�te");
		hm.put(410, "Celui qui draguait au large");
		hm.put(411, "Celui qui posait une question embarrassante");
		hm.put(412, "Celui qui gagnait les paris");
		hm.put(413, "Celui qui se gourait du tout au tout");
		hm.put(414, "Celui qui n'avait pas le moral");
		hm.put(415, "Celui qui jouait au rugby");
		hm.put(416, "Celui qui participait � une f�te bidon");
		hm.put(417, "Celui qui avait la cha�ne porno");
		hm.put(418, "Celui qui cherche un pr�nom");
		hm.put(419, "Celui qui faisait de grands projets");
		hm.put(420, "Celui qui va se marier");
		hm.put(421, "Celui qui envoie l'invitation");
		hm.put(422, "Celui qui �tait le pire t�moin du monde");
		hm.put(423, "Celui qui se marie - partie 1");
		hm.put(424, "Celui qui se marie - partie 2");

		Set<Entry<Integer, String>> setHm = hm.entrySet();
		Iterator<Entry<Integer, String>> it = setHm.iterator();
		while (it.hasNext()) {
			Entry<Integer, String> e = it.next();
			int seasonIndex = (e.getKey() / 100) - 1;
			System.out.println("Ep : " + e.getKey() % 100 + " : " + e.getValue() + " / season index : " + seasonIndex);
			Episode ep = new Episode();
			ep.setNumber(e.getKey() % 100);
			ep.setTitle(e.getValue());

			friendsSeasons.get(seasonIndex).addEpisode(ep); 

			em.persist(ep);

		}
	}
}