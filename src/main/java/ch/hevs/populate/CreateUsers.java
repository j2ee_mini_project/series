package ch.hevs.populate;

import javax.persistence.EntityManager;

import ch.hevs.businessobject.User;

public class CreateUsers {
	
	public static void populateUsers(EntityManager em) {
		
		User jack = new User();
		jack.setEmail("jack.h@hevs.com");
		jack.setFirstname("Jacques");
		jack.setLastname("Herren");
		jack.setUsername("jh");
		
		em.persist(jack);
		
		User sam = new User();
		sam.setEmail("sam.c@hevs.com");
		sam.setFirstname("Samuel");
		sam.setLastname("Coppey");
		sam.setUsername("sc");
		
		em.persist(sam);
		
		User visitor = new User();
		visitor.setEmail("visitor@hevs.com");
		visitor.setFirstname("Visitor");
		visitor.setLastname("Unknown");
		visitor.setUsername("visitor");
		
		em.persist(visitor);
		
	}
	
	

}
