package ch.hevs.populate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import ch.hevs.businessobject.Episode;
import ch.hevs.businessobject.Library;
import ch.hevs.businessobject.Season;
import ch.hevs.businessobject.Serie;
import ch.hevs.businessobject.User;
import ch.hevs.serieservice.TvSerieBean;
import ch.hevs.serieservice.TvSeries;

public class GoT {


	public static void  populateGoT(EntityManager em) {


		/*
		 * game of thrones
		 */
		// SERIE
		Serie serie = new Serie();
		serie.setName("Game of Thrones");
		em.persist(serie);

		// SEASONS
		int seasonYear[] = { 2011, 2012, 2013, 2014,2015,2016,2017,2019 };
		List<Season> seasons = new ArrayList<Season>();

		for (int i = 0; i < seasonYear.length; i++) {
			int SeasonNumber = i + 1;
			Season s = new Season();
			s.setNumber(SeasonNumber);
			s.setYear(seasonYear[i]);
			seasons.add(s);
			serie.addSeason(seasons.get(i));
			em.persist(s);
		}

		// EPISODES
		Map<Integer, String> hm = new LinkedHashMap<>();
		hm.put(101,"Winter Is Coming");
		hm.put(102,"The Kingsroad");
		hm.put(103,"Lord Snow");
		hm.put(104,"Cripples, Bastards, and Broken Things");
		hm.put(105,"The Wolf and the Lion");
		hm.put(106,"A Golden Crown");
		hm.put(107,"You Win or You Die");
		hm.put(108,"The Pointy End");
		hm.put(109,"Baelor");
		hm.put(110,"Fire and Blood");
		hm.put(201,"The North Remembers");
		hm.put(202,"The Night Lands");
		hm.put(203,"What Is Dead May Never Die");
		hm.put(204,"Garden of Bones");
		hm.put(205,"The Ghost of Harrenhal");
		hm.put(206,"The Old Gods and the New");
		hm.put(207,"A Man Without Honor");
		hm.put(208,"The Prince of Winterfell");
		hm.put(209,"Blackwater");
		hm.put(210,"Valar Morghulis");
		hm.put(301,"Valar Dohaeris");
		hm.put(302,"Dark Wings, Dark Words");
		hm.put(303,"Walk of Punishment");
		hm.put(304,"And Now His Watch Is Ended");
		hm.put(305,"Kissed by Fire");
		hm.put(306,"The Climb");
		hm.put(307,"The Bear and the Maiden Fair");
		hm.put(308,"Second Sons");
		hm.put(309,"The Rains of Castamere");
		hm.put(310,"Mhysa");
		hm.put(401,"Two Swords");
		hm.put(402,"The Lion and the Rose");
		hm.put(403,"Breaker of Chains");
		hm.put(404,"Oathkeeper");
		hm.put(405,"First of His Name");
		hm.put(406,"The Laws of Gods and Men");
		hm.put(407,"Mockingbird");
		hm.put(408,"The Mountain and the Viper");
		hm.put(409,"The Watchers on the Wall");
		hm.put(410,"The Children");
		hm.put(501,"The Wars to Come");
		hm.put(502,"The House of Black and White");
		hm.put(503,"High Sparrow");
		hm.put(504,"Sons of the Harpy");
		hm.put(505,"Kill the Boy");
		hm.put(506,"Unbowed, Unbent, Unbroken");
		hm.put(507,"The Gift");
		hm.put(508,"Hardhome");
		hm.put(509,"The Dance of Dragons");
		hm.put(510,"Mother's Mercy");
		hm.put(601,"The Red Woman");
		hm.put(602,"Home");
		hm.put(603,"Oathbreaker");
		hm.put(604,"Book of the Stranger");
		hm.put(605,"The Door");
		hm.put(606,"Blood of My Blood");
		hm.put(607,"The Broken Man");
		hm.put(608,"No One");
		hm.put(609,"Battle of the Bastards");
		hm.put(610,"The Winds of Winter");
		hm.put(701,"Comming soon...");
		hm.put(702,"Comming soon...");
		hm.put(703,"Comming soon...");
		hm.put(704,"Comming soon...");
		hm.put(705,"Comming soon...");
		hm.put(706,"Comming soon...");
		hm.put(707,"Comming soon...");

		Set<Entry<Integer, String>> setHm = hm.entrySet();
		Iterator<Entry<Integer, String>> it = setHm.iterator();
		while (it.hasNext()) {
			Entry<Integer, String> e = it.next();
			int seasonIndex = (e.getKey() / 100) - 1;
			System.out.println("Ep : " + e.getKey() % 100 + " : " + e.getValue() + " / season index : " + seasonIndex);
			Episode ep = new Episode();
			ep.setNumber(e.getKey() % 100);
			ep.setTitle(e.getValue());

			seasons.get(seasonIndex).addEpisode(ep); 

			em.persist(ep);

		}
	}
}