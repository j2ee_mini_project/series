package ch.hevs.businessobject;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "EPISODES")
public class Episode {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private int number;
	private String title;
	
	@ManyToOne
	@JoinColumn(name = "FK_SEASON")
	private Season season;
	
	@OneToMany(mappedBy = "episode")
	private Set<Library> libraries  = new HashSet<Library>();
	
	
	public Set<Library> getLibraries() {
		return libraries;
	}
	public void setLibraries(Set<Library> libraries) {
		this.libraries = libraries;
	}
	public void addLibrary(Library l){
		this.libraries.add(l);
		l.setEpisode(this);
	}
	
	public Episode() {

	}
	
	@Override
	public String toString() {
		return "Episode [id=" + id + ", number=" + number + ", title=" + title + ", season=" + season + ", libraries="
				+ libraries + "]";
	}
	
	// *** GETTERS ***
	public long getId() {
		return id;
	}
	public int getNumber() {
		return number;
	}
	
	public String getTitle() {
		return title;
	}

	public Season getSeason() {
		return season;
	}

	// *** SETTERS ***
	public void setId(long id) {
		this.id = id;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setSeason(Season season) {
		this.season = season;
	}

}
