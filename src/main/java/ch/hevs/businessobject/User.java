package ch.hevs.businessobject;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USERS")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	private String firstname;
	private String lastname;
	private String email;
	private String result;
	
	 @Column(unique=true)
	private String username;
	 	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	private String pwd;
	
	@OneToMany(mappedBy = "user")
	private Set<Library> libraries  = new HashSet<Library>();
	
	public Set<Library> getLibraries() {
		return libraries;
	}
	public void setLibraries(Set<Library> libraries) {
		this.libraries = libraries;
	}
	
	
	public void addLibrary(Library l){
		this.libraries.add(l);
		l.setUser(this);
	}
	

	
	

}
