package ch.hevs.businessobject;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.Table;


@Entity
@Table(name = "SEASONS")
public class Season {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private int number;
	private int year;
	
	/// relations
	@ManyToOne
	@JoinColumn(name = "FK_SERIE")
	private Serie serie;
	
	
	@OneToMany(mappedBy = "season")
	private List<Episode> episodes;

	// constructors
	public Season()
	{
		this.episodes = new ArrayList<Episode>();
	}

	
	// helpermethods
	public void addEpisode(Episode e){
		
		e.setSeason(this);
		episodes.add(e);
			
	}
	

	public List<Episode> getEpisodes() {
		return episodes;
	}


	public void setEpisodes(List<Episode> episodes) {
		this.episodes = episodes;
	}

	// id 
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	// number
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
	// year
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	// serie
	public Serie getSerie() {
		return serie;
	}
	public void setSerie(Serie serie) {
		this.serie = serie;
	}
	
	// methods
	
	@PostPersist
	public void acknowledgePersist() {
		System.out.println("Season persisted!!!");
	}


	
}