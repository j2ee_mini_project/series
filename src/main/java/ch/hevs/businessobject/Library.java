package ch.hevs.businessobject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "LIBRARIES")
public class Library {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	public long getId() {
		return id;
	}
	
	public Library(){
		
	}
	
	public Library(User u, Episode e, boolean b){
		this.setUser(u);
		this.setEpisode(e);
		this.setWatched(b);
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "FK_USER")
	private User user;
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@ManyToOne
	@JoinColumn(name = "FK_EPISODE")
	private Episode episode;
	public Episode getEpisode() {
		return episode;
	}

	public void setEpisode(Episode episode) {
		this.episode = episode;
	}
	

	private boolean watched;
	public boolean isWatched() {
		return watched;
	}


	public void setWatched(boolean watched) {
		this.watched = watched;
	}
	
}
