package ch.hevs.businessobject;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "SERIES")
public class Serie {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private String name;
	
	// relations
	@OneToMany(mappedBy = "serie", cascade=CascadeType.ALL)
	private List<Season> seasons;
	
	// constructors
	public Serie(){
		this.seasons = new ArrayList<Season>();
	}
	public Serie(String name) {
		this.seasons = new ArrayList<Season>();
		this.name = name;
	}
	
	//Getter Setter
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Season> getSeasons() {
		return seasons;
	}
	public void setSeasons(List<Season> seasons) {
		this.seasons = seasons;
	}
	// helpermethods
	public void addSeason(Season s){
		
		//System.out.println("count seasons in serie: " + seasons.size());
		s.setSerie(this);
		seasons.add(s);
		
	}

}