package ch.hevs.serieservice;

import java.util.List;

import javax.ejb.Local;

import ch.hevs.businessobject.Episode;
import ch.hevs.businessobject.Library;
import ch.hevs.businessobject.Season;
import ch.hevs.businessobject.Serie;
import ch.hevs.businessobject.User;

@Local
public interface TvSeries {

	// Count state user
	public int countAllEpisodesByUserId(User user);
	public int countWatchedEpisodesByUserId(User user);
	public int countUnwatchedEpisodesByUserId(User user);
	public int countSeasonBySerie(Serie serie);
	public int countEpisodeBySerie(Serie serie);
	public int countEpisodeBySeason(Season season);
	public int countWatchedEpisodesByUserIdBySerie(User user, Serie serie);
	public int countUnWatchedEpisodesByUserIdBySerie(User user, Serie serie);
	public int countAllEpisodesByUserIdBySerie(User user, Serie serie);
	public List<Serie> getSeriesByName(String name);
	public List<Serie> getAllSeriesByUser(User user);
	public List<Season> getAllSeasonsByUserAndBySerie(User user, Serie serie);
	public List<Episode> getAllEpisodeByUserAndBySeason(User user, Season season);
	public List<Episode> getAllEpisodeBySerie(Serie serie);
	public List<Library> getLibrariesByUserAndBySeason(User user, Season season);
	public List<Library> getLibrariesByUserAndBySerie(User user, Serie serie);
	public User currentUser() ;
	public List<User> getUsers();
	public void populate();
	public String changeFollowStatusSerie(List<Library> libraries);
	public String changeWatchedStatus(Library library);
	public String removeFollowStatusSerie(List<Library> libraries);
	public int countWatchedEpisodesByUserIdBySeason(User user, Season season);

}
