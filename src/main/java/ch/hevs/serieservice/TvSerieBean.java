package ch.hevs.serieservice;

import java.util.List;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import ch.hevs.businessobject.Episode;
import ch.hevs.businessobject.Library;
import ch.hevs.businessobject.Season;
import ch.hevs.businessobject.Serie;
import ch.hevs.businessobject.User;
import ch.hevs.populate.CreateUsers;
import ch.hevs.populate.Friends;
import ch.hevs.populate.GoT;
import ch.hevs.populate.Lost;


@Stateful
@RolesAllowed(value = { "users", "visitors" })
public class TvSerieBean implements TvSeries {
	
	@PersistenceContext(name = "seriesPU", type=PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	@Resource 
	private SessionContext ctx;

	
	public void populate() {
		CreateUsers.populateUsers(em);
		Lost.populateLost(em);
		Friends.populateFriends(em);		
		GoT.populateGoT(em);
	}

	public List<Episode> getEpisodesBySeason(long seasonId) {
		return (List<Episode>) em.createQuery("FROM Season s where s.id=:id").setParameter("id", seasonId).getResultList();
	}

	@Override
	public int countAllEpisodesByUserId(User user) {
		int count = 0;
		count = ((Number)em.createQuery("Select Count(l) from Library l where l.user=:user").setParameter("user", user).getSingleResult()).intValue();
		return count;
	}

	@Override
	public int countWatchedEpisodesByUserId(User user) {
		int count = 0;
		
		Query query = em.createQuery("SELECT Count(l) FROM Library l WHERE l.user=:user and l.watched=:b");
		query.setParameter("user", user);
		query.setParameter("b", true);
		
		count = ((Number) query.getSingleResult()).intValue();
		
	
		return count;
	}

	public Episode getEpisode(long episodeId) {
		
		return (Episode) em.createQuery("FROM Episode e where e.id=:id").setParameter("id", episodeId).getSingleResult();
	}

	@Override
	public List<User> getUsers() {
		List<User> users ;
		users = (List<User>) em.createQuery("FROM User").getResultList();
		return users;
	}

	@Override
	public int countSeasonBySerie(Serie serie) {
		int count = 0;
		count = ((Number)em.createQuery("Select Count(s) from Season s where s.serie=:serie").setParameter("serie", serie).getSingleResult()).intValue();
		return count;
	}

	@Override
	public int countEpisodeBySerie(Serie serie) {
		int count = 0;
		count = ((Number)em.createQuery("Select Count(e) from Episode e LEFT JOIN  e.season s where s.serie=:serie").setParameter("serie", serie).getSingleResult()).intValue();
		return count;
	}

	@Override
	public int countUnwatchedEpisodesByUserId(User user) {
		int count = 0;
		
		Query query = em.createQuery("SELECT Count(l) FROM Library l WHERE l.user=:user and l.watched=:b");
		query.setParameter("user", user);
		query.setParameter("b", false);
		
		count = ((Number) query.getSingleResult()).intValue();
		
		return count;
	}
	
	@Override
	public int countWatchedEpisodesByUserIdBySerie(User user, Serie serie) {
		int count = 0;
		
		Query query = em.createQuery("SELECT Count(l) FROM Library l left join l.episode e left join e.season o left join o.serie WHERE l.user=:user and l.watched=:b and o.serie=:serie");
		query.setParameter("serie", serie);
		query.setParameter("user", user);
		query.setParameter("b", true);
		
		count = ((Number) query.getSingleResult()).intValue();
		
		return count;
	}
	
	@Override
	public int countWatchedEpisodesByUserIdBySeason(User user, Season season) {
		int count = 0;
		
		Query query = em.createQuery("SELECT Count(l) FROM Library l left join l.episode e left join e.season o WHERE l.user=:user and l.watched=:b and e.season=:season");
		query.setParameter("season", season);
		query.setParameter("user", user);
		query.setParameter("b", true);
		
		count = ((Number) query.getSingleResult()).intValue();
		
		return count;
	}
	
	@Override
	public int countAllEpisodesByUserIdBySerie(User user, Serie serie) {
		int count = 0;
		
		Query query = em.createQuery("SELECT Count(l) FROM Library l left join l.episode e left join e.season o left join o.serie WHERE l.user=:user and o.serie=:serie");
		query.setParameter("serie", serie);
		query.setParameter("user", user);
		
		count = ((Number) query.getSingleResult()).intValue();
		
		return count;
	}
	
	@Override
	public int countEpisodeBySeason(Season season) {
		int count = 0;
		count = ((Number)em.createQuery("Select Count(e) from Episode e where e.season=:season").setParameter("season", season).getSingleResult()).intValue();
		return count;
	}

	@Override
	public List<Serie> getSeriesByName(String name) {
		return (List<Serie>) em.createQuery("FROM Serie s where UPPER(s.name) like :search").setParameter("search",  "%"+name.toUpperCase()+"%").getResultList();

	}

	@Override
	public List<Serie> getAllSeriesByUser(User user) {
		return (List<Serie>) em.createQuery("SELECT DISTINCT s FROM Library l left join l.episode e left join e.season o left join o.serie s where l.user =:user").setParameter("user",  user).getResultList();
	}

	@Override
	public List<Season> getAllSeasonsByUserAndBySerie(User user, Serie serie) {
		return (List<Season>) em.createQuery("SELECT DISTINCT o FROM Library l LEFT JOIN l.episode e LEFT JOIN e.season o where l.user =:user and o.serie=:serie").setParameter("user",  user).setParameter("serie",  serie).getResultList();
	}

	@Override
	public List<Episode> getAllEpisodeByUserAndBySeason(User user, Season season) {
		return (List<Episode>) em.createQuery("SELECT DISTINCT e FROM Library l LEFT JOIN l.episode e where l.user =:user and e.season=:season").setParameter("user",  user).setParameter("season",  season).getResultList();
	}

	@Override
	public int countUnWatchedEpisodesByUserIdBySerie(User user, Serie serie) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String changeFollowStatusSerie(List<Library> libraries)
	{
		
		for(Library l : libraries){
			em.persist(l);
		}
		
		String transactionResult="Success!";
		if (ctx.isCallerInRole("visitors")) { // verify that the caller has the correct role if the amount is above 100CHF
			transactionResult="Error: " + ctx.getCallerPrincipal().getName() + " is not allowed to follow any serie.";
			
			System.out.println(transactionResult);
			ctx.setRollbackOnly(); // rollback if the caller is not a banker
		}
	

		return transactionResult;
	}

	@Override
	public String removeFollowStatusSerie(List<Library> libraries)
	{
		for(Library l : libraries){
			em.remove(l);
		}
		return "Success !";
	}
	@Override
	public String changeWatchedStatus(Library library) {
		// TODO Auto-generated method stub
		
		em.persist(library);
		return "Success !";
	}

	@Override
	public List<Episode> getAllEpisodeBySerie(Serie serie) {
		// TODO Auto-generated method stub
		return (List<Episode>) em.createQuery("SELECT DISTINCT e FROM Episode e LEFT JOIN e.season o LEFT JOIN o.serie s where o.serie=:serie").setParameter("serie",  serie).getResultList();
	}

	@Override
	public List<Library> getLibrariesByUserAndBySeason(User user, Season season) {
		return (List<Library>) em.createQuery("SELECT DISTINCT l FROM Library l LEFT JOIN l.episode e where l.user =:user and e.season=:season").setParameter("user",  user).setParameter("season",  season).getResultList();
	}

	@Override
	public User currentUser() {
		String username = ctx.getCallerPrincipal().getName();
		
		return (User) em.createQuery("SELECT u FROM User u WHERE u.username=:username").setParameter("username", username).getSingleResult();
		
	}

	@Override
	public List<Library> getLibrariesByUserAndBySerie(User user, Serie serie) {
		return (List<Library>) em.createQuery("SELECT DISTINCT l FROM Library l LEFT JOIN l.episode e LEFT JOIN e.season o where l.user =:user and o.serie=:serie").setParameter("user",  user).setParameter("serie",  serie).getResultList();
	}

}
