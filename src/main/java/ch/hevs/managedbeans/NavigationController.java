package ch.hevs.managedbeans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "navigationController", eager = true)
@RequestScoped

public class NavigationController implements Serializable {
	private static final long serialVersionUID = 1L;  
	@ManagedProperty(value = "#{param.pageId}")    
	private String pageId; 

	public String librairie() {       
		return "LibrairiePage";    
	}  
	public String profil() {       
		return "ProfilPage";    
	} 
	public String settings() {       
		return "SettingsPage";    
	} 
	public String search() {
		return "SearchPage";
	}
	public String series() {
		return "SeriesPage";
	}
	public String seasons() {
		return "SeasonPage.xhtml";
	}
	public String getPageId() {       
		return pageId;    
	}  
	public String validatePwd()
	{
		return "ProfilPage";
	}

	public void setPageId(String pageId) {       
		this.pageId = pageId;   
	} 

}
