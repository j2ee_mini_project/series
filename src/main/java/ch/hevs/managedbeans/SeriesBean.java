package ch.hevs.managedbeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.PieChartModel;

import ch.hevs.businessobject.Episode;
import ch.hevs.businessobject.Library;
import ch.hevs.businessobject.Season;
import ch.hevs.businessobject.Serie;
import ch.hevs.businessobject.User;
import ch.hevs.serieservice.TvSeries;


public class SeriesBean {

	private List<Episode> allEpisodesBySerie;
	private List<Episode> watchedUserEpisodes;
	private List<Episode> unwatchedUserEpisodes;
	private List<Episode> seriesEpisodes;
	private List<Episode> allEpisodeByUserAndBySeason;
	private List<Season> allSeasonBySeries;
	private List<Library> allLibrariesByUserAndBySeason;
	private List<Library> allLibrariesByUserAndBySerie;
	private List<Serie> allSeriesByUser;
	private List<Serie> allSeriesByName;
	private int nbWatchedEpisodesByUserBySerie;
	private int nbAllEpisodesByUserBySerie;
	private int nbUnWatchedEpisodesByUserBySerie;
	private int nbSeasonBySerie;
	private int nbEpisodesBySerie;	
	private int nbUserAllEpisode;
	private int nbWatchedUserEpisode;
	private int nbUnwatchedUserEpisode;
	private int nbWatchedEpisodeBySeason;
	private PieChartModel pieModel;
	private PieChartModel pieModelSeason;
	private String valueTextButton;
	private String classButton;
	private User user;
	private Serie selectedSerie;
	private Season selectedSeason;
	private Episode selectedEpisode;
	private Library selectedLibrary;
	private TvSeries tvseries;


	@PostConstruct
	public void initialize() throws NamingException {
		//JNDI
		InitialContext ctx = new InitialContext();
		tvseries = (TvSeries) ctx.lookup("java:global/MiniProject-0.0.1-SNAPSHOT/TvSerieBean!ch.hevs.serieservice.TvSeries");
		
		populate();
		createPieModels();
	}

	public String getClassButton(Library l) {
		if(l.isWatched())
			setClassButton("btn-primary");
		else
			setClassButton("btn-success");
		return classButton;
	}
	
	public int getNbWatchedEpisodeBySeason(Season season) {
		nbWatchedEpisodeBySeason = tvseries.countWatchedEpisodesByUserIdBySeason(user, season);
		return nbWatchedEpisodeBySeason;
	}
	public void setNbWatchedEpisodeBySeason(int nbWatchedEpisodeBySeason) {
		this.nbWatchedEpisodeBySeason = nbWatchedEpisodeBySeason;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	public String getClassButton() {
		return classButton;
	}

	public void setClassButton(String classButton) {
		this.classButton = classButton;
	}

	public String getValueTextButton() {
				
		return valueTextButton;
	}
	
	public String getValueTextButton(Library l) {
		if(l.isWatched())
			setValueTextButton("Watched");
		else
			setValueTextButton("To watch");
		
		return valueTextButton;
	}

	public void setValueTextButton(String valueTextButton) {
		this.valueTextButton = valueTextButton;
	}
	
	public void populate(){
		tvseries.populate();

		this.user = tvseries.currentUser();
		System.out.println("username = " + user.getUsername());
		this.nbUserAllEpisode = tvseries.countAllEpisodesByUserId(user);
		this.nbWatchedUserEpisode = tvseries.countWatchedEpisodesByUserId(user);
		this.nbUnwatchedUserEpisode = this.nbUserAllEpisode - this.nbWatchedUserEpisode;
	}
	
	private void createPieModels() {
        createPieModel1();
        createPieModel2();
    }
     
    private void createPieModel1() {
    		
	        this.pieModel = new PieChartModel();
	        this.pieModel.setLegendPosition("s");
	        this.pieModel.setDiameter(150);

    }
    
    private void createPieModel2() {
		
        this.pieModelSeason = new PieChartModel();
        this.pieModelSeason.setLegendPosition("s");
        this.pieModelSeason.setDiameter(50);

}
    
    public void itemSelect(ItemSelectEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item selected",
                        "Item Index: " + event.getItemIndex() + ", Series Index:" + event.getSeriesIndex());
         
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
	
	public PieChartModel getPieModel() {
		
		int watched = getNbWatchedUserEpisode();
		int towatch = getNbUnwatchedUserEpisode();

         
        this.pieModel.set("Watched", watched);
        this.pieModel.set("To watch", towatch);
		
		
		return pieModel;
	}
	
	public PieChartModel getPieModelSeason(Season season) {

		int watched = getNbWatchedEpisodeBySeason(season);
		int nbTotalEpisode = tvseries.countEpisodeBySeason(season);
		
		System.out.println(nbTotalEpisode);
		
		
		int towatch = nbTotalEpisode-watched;
		
		System.out.println(towatch);


         
        this.pieModelSeason.set("Watched", watched);
        this.pieModelSeason.set("To watch", towatch);
		
		
		return pieModelSeason;
	}

	public PieChartModel getPieModelSeason() {
		return pieModelSeason;
	}



	public void setPieModel(PieChartModel pieModel) {
		this.pieModel = pieModel;
	}

	public int getNbEpisodesBySerie(Serie serie) {
		nbEpisodesBySerie = tvseries.countEpisodeBySerie(serie);
		return nbEpisodesBySerie;
	}

	public void setNbEpisodesBySerie(int nbEpisodesBySerie) {
		this.nbEpisodesBySerie = nbEpisodesBySerie;
	}

	public int getNbSeasonBySerie(Serie serie) {
		nbSeasonBySerie = tvseries.countSeasonBySerie(serie);
		return nbSeasonBySerie;
	}

	public void setNbSeasonBySerie(int nbSeasonBySerie) {
		this.nbSeasonBySerie = nbSeasonBySerie;
	}

	public List<Serie> getAllSeriesByName() {
		return allSeriesByName;
	}

	public void setAllSeriesByName(List<Serie> allSeriesByName) {
		this.allSeriesByName = allSeriesByName;
	}

	public List<Episode> getAllEpisodesBySerie() {
		allEpisodesBySerie = tvseries.getAllEpisodeBySerie(selectedSerie);
		return allEpisodesBySerie;
	}

	public void setAllEpisodesBySerie(List<Episode> allEpisodesBySerie) {
		this.allEpisodesBySerie = allEpisodesBySerie;
	}

	private String transactionResult;


	public String getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public Library getSelectedLibrary() {
		return selectedLibrary;
	}

	public void setSelectedLibrary(Library selectedLibrary) {
		this.selectedLibrary = selectedLibrary;
	}

	public List<Library> getAllLibrariesByUserAndBySeason() {
		this.allLibrariesByUserAndBySeason = tvseries.getLibrariesByUserAndBySeason(user, selectedSeason);
		return allLibrariesByUserAndBySeason;
	}

	public void setAllLibrariesByUserAndBySeason(List<Library> allLibrariesByUserAndBySeason) {
		this.allLibrariesByUserAndBySeason = allLibrariesByUserAndBySeason;
	}
	
	public List<Library> getAllLibrariesByUserAndBySerie() {
		this.allLibrariesByUserAndBySerie = tvseries.getLibrariesByUserAndBySerie(user, selectedSerie);
		return allLibrariesByUserAndBySerie;
	}

	public void setAllLibrariesByUserAndBySerie(List<Library> allLibrariesByUserAndBySerie) {
		this.allLibrariesByUserAndBySerie = allLibrariesByUserAndBySerie;
	}

	public List<Episode> getAllEpisodeByUserAndBySeason() {
		allEpisodeByUserAndBySeason = tvseries.getAllEpisodeByUserAndBySeason(user, selectedSeason);
		return allEpisodeByUserAndBySeason;
	}

	public void setAllEpisodeByUserAndBySeason(List<Episode> allEpisodeByUserAndBySeason) {
		this.allEpisodeByUserAndBySeason = allEpisodeByUserAndBySeason;
	}

	public int getNbUnWatchedEpisodesByUserBySerie(Serie serie) {
		this.nbAllEpisodesByUserBySerie = getNbAllEpisodesByUserBySerie(serie);
		this.nbWatchedEpisodesByUserBySerie = getNbWatchedEpisodesByUserBySerie(serie);
		
		this.nbUnWatchedEpisodesByUserBySerie = nbAllEpisodesByUserBySerie - nbWatchedEpisodesByUserBySerie;
		return nbUnWatchedEpisodesByUserBySerie;
	}

	public void setNbUnWatchedEpisodesByUserBySerie(int nbUnWatchedEpisodesByUserBySerie) {
		this.nbUnWatchedEpisodesByUserBySerie = nbUnWatchedEpisodesByUserBySerie;
	}

	public int getNbAllEpisodesByUserBySerie(Serie serie) {
		nbAllEpisodesByUserBySerie = tvseries.countAllEpisodesByUserIdBySerie(user, serie);
		return nbAllEpisodesByUserBySerie;
	}

	public void setNbAllEpisodesByUserBySerie(int nbAllEpisodesByUserBySerie) {
		this.nbAllEpisodesByUserBySerie = nbAllEpisodesByUserBySerie;
	}

	public int getNbWatchedEpisodesByUserBySerie(Serie serie) {
		nbWatchedEpisodesByUserBySerie = tvseries.countWatchedEpisodesByUserIdBySerie(user,serie);
		return nbWatchedEpisodesByUserBySerie;
	}

	public void setNbWatchedEpisodesByUserBySerie(int nbWatchedEpisodesByUserBySerie) {
		this.nbWatchedEpisodesByUserBySerie = nbWatchedEpisodesByUserBySerie;
	}

	public List<Serie> getAllSeriesByUser() {
		allSeriesByUser = tvseries.getAllSeriesByUser(user);
		return allSeriesByUser;
	}

	public void setAllSeriesByUser(List<Serie> allSeriesByUser) {
		this.allSeriesByUser = allSeriesByUser;
	}

	public Serie getSelectedSerie() {
		return selectedSerie;
	}

	public void setSelectedSerie(Serie selectedSerie) {
		this.selectedSerie = selectedSerie;
	}

	public Season getSelectedSeason() {
		return selectedSeason;
	}

	public void setSelectedSeason(Season selectedSeason) {
		this.selectedSeason = selectedSeason;
	}

	public List<Season> getAllSeasonBySeries() {
		allSeasonBySeries = tvseries.getAllSeasonsByUserAndBySerie(user, selectedSerie);
		return allSeasonBySeries;
	}

	public String openSerie(){

		allSeasonBySeries = tvseries.getAllSeasonsByUserAndBySerie(user, selectedSerie);
		return "SeasonPage";
	}
	
	public String searchSeries(String name){
		
		allSeriesByName = tvseries.getSeriesByName(name);
		return "SearchPage";
	}

	public String openSeason(){

		allEpisodeByUserAndBySeason = tvseries.getAllEpisodeByUserAndBySeason(user, selectedSeason);
		return "SeriesPage";
	}

	public void setAllSeasonBySeries(List<Season> allSeasonBySeries) {
		this.allSeasonBySeries = allSeasonBySeries;
	}
	
	

	public void changeWatchStatus() {
		selectedLibrary.setWatched(!selectedLibrary.isWatched());
		this.transactionResult = tvseries.changeWatchedStatus(selectedLibrary);
	}

	public void followSerie() {

		List<Library> libs = new ArrayList<Library>();
		allEpisodesBySerie = tvseries.getAllEpisodeBySerie(selectedSerie);
		for(Episode e : allEpisodesBySerie)
		{
			Library l = new Library(user,e,false);
			libs.add(l);
		}
		this.transactionResult = tvseries.changeFollowStatusSerie(libs);
	}
	
	public void removeSerie() {

		allLibrariesByUserAndBySerie = tvseries.getLibrariesByUserAndBySerie(user, selectedSerie);
		this.transactionResult = tvseries.removeFollowStatusSerie(allLibrariesByUserAndBySerie);
		
	}

	public int getNbUserAllEpisode() {
		this.nbUserAllEpisode = tvseries.countAllEpisodesByUserId(user);
		return nbUserAllEpisode;
	}

	public void setNbUserAllEpisode(int nbUserAllEpisode) {
		this.nbUserAllEpisode = nbUserAllEpisode;
	}

	public List<Episode> getWatchedUserEpisodes() {
		return watchedUserEpisodes;
	}

	public void setWatchedUserEpisodes(List<Episode> watchedUserEpisodes) {
		this.watchedUserEpisodes = watchedUserEpisodes;
	}

	public List<Episode> getUnwatchedUserEpisodes() {
		return unwatchedUserEpisodes;
	}

	public void setUnwatchedUserEpisodes(List<Episode> unwatchedUserEpisodes) {
		this.unwatchedUserEpisodes = unwatchedUserEpisodes;
	}

	public int getNbWatchedUserEpisode() {
		this.nbWatchedUserEpisode = tvseries.countWatchedEpisodesByUserId(user);
		return nbWatchedUserEpisode;
	}

	public void setNbWatchedUserEpisode(int nbWatchedUserEpisode) {
		this.nbWatchedUserEpisode = nbWatchedUserEpisode;
	}

	public int getNbUnwatchedUserEpisode() {
		this.nbUnwatchedUserEpisode = tvseries.countUnwatchedEpisodesByUserId(user);
		return nbUnwatchedUserEpisode;
	}

	public void setNbUnwatchedUserEpisode(int nbUnwatchedUserEpisode) {
		this.nbUnwatchedUserEpisode = nbUnwatchedUserEpisode;
	}

	public List<Episode> getAllEpisodeFromSerie() {
		return seriesEpisodes;
	}

	public Episode getSelectedEpisode() {
		return selectedEpisode;
	}

	public void setSelectedEpisode(Episode selectedEpisode) {
		this.selectedEpisode = selectedEpisode;
	}	
}
