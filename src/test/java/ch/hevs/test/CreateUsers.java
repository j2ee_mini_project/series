package ch.hevs.test;

import javax.persistence.EntityManager;

import ch.hevs.businessobject.User;

public class CreateUsers {
	
	private EntityManager em;
	
	public CreateUsers(EntityManager em) {
		this.em = em;
		
		User jack = new User();
		jack.setEmail("jack.h@hevs.com");
		jack.setFirstname("Jacques");
		jack.setLastname("Herren");
		jack.setUsername("jh");
		
		em.persist(jack);
		
		User sam = new User();
		sam.setEmail("sam.c@hevs.com");
		sam.setFirstname("Samuel");
		sam.setLastname("Coppey");
		sam.setUsername("sc");
		
		em.persist(sam);
		
	}
	
	

}
