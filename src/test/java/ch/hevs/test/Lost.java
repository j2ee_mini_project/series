package ch.hevs.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import ch.hevs.businessobject.Episode;
import ch.hevs.businessobject.Library;
import ch.hevs.businessobject.Season;
import ch.hevs.businessobject.Serie;
import ch.hevs.businessobject.User;

public class Lost {
	private EntityManager em;
	public Lost(EntityManager em){
		
		this.em = em;
			
			
			
			
			/*
			 * LOST 
			 */
			// SERIE
			Serie lost = new Serie();
			lost.setName("Lost");
			em.persist(lost);
			
			// SEASONS
			int lostSeasonYear[] = {2004,2005,2006,2007,2009,2010};
			List<Season> lostSeasons = new ArrayList<Season>();
			
			for(int i = 0;i<6;i++){
				int SeasonNumber = i+1;
				Season s = new Season();
				s.setNumber(SeasonNumber);
				s.setYear(lostSeasonYear[i]);
				lostSeasons.add(s);
				lost.addSeason(lostSeasons.get(i));
				em.persist(s);
			}
			
			// EPISODES
			Map<Integer, String> hm = new LinkedHashMap<>();
			hm.put(101,"Le R�veil, premi�re partie�(Pilot: Part One)");
			hm.put(102,"Le R�veil, deuxi�me partie�(Pilot: Part Two)");
			hm.put(103,"Le Nouveau D�part�(Tabula Rasa)");
			hm.put(104,"Les Pieds sur terre�(Walkabout)");
			hm.put(105,"� la recherche du p�re�(White Rabbit)");
			hm.put(106,"Regard vers l'ouest�(House of the Rising Sun)");
			hm.put(107,"Le Papillon de nuit�(The Moth)");
			hm.put(108,"Transfert d'identit�(Confidence Man)");
			hm.put(109,"Le Choix du soldat�(Solitary)");
			hm.put(110,"La Force du destin�(Raised by Another)");
			hm.put(111,"Les D�mons int�rieurs�(All the Best Cowboys Have Daddy Issues)");
			hm.put(112,"L'Objet de tous les d�sirs�(Whatever the Case May Be)");
			hm.put(113,"Le c�ur a ses raisons��(Hearts and Minds)");
			hm.put(114,"Au nom du fils�(Special)");
			hm.put(115,"� la d�rive�(Homecoming)");
			hm.put(116,"Le Prix de la vengeance�(Outlaws)");
			hm.put(117,"Le Mur du silence�(� in Translation)");
			hm.put(118,"La Loi des nombres�(Numbers)");
			hm.put(119,"Tomb� du ciel�(Deus ex machina)");
			hm.put(120,"Pour le meilleur et pour le pire�(Do No Harm)");
			hm.put(121,"Elle ou lui�/�Pour le bien de tous�(The Greater Good)");
			hm.put(122,"�ternelle fugitive�(Born to Run)");
			hm.put(123,"L'Exode, premi�re partie�(Exodus:�Part One)");
			hm.put(124,"L'Exode, deuxi�me partie�(Exodus:�Part Two)");
			hm.put(125,"L'Exode, troisi�me partie�(Exodus:�Part Three)");
			hm.put(201,"La Descente�(Man of Science, Man of Faith)");
			hm.put(202,"Seuls au monde�(Adrift)");
			hm.put(203,"108 minutes�(Orientation)");
			hm.put(204,"Le mal-aim�(Everybody Hates Hugo)");
			hm.put(205,"Retrouv�s��(... and Found)");
			hm.put(206,"Abandonn�e�(Abandoned)");
			hm.put(207,"Les Autres 48 jours�(The Other 48 Days)");
			hm.put(208,"La Rencontre�/�Repr�sailles�(Collision)");
			hm.put(209,"Message personnel�/�Le Mal dans le sang�(What Kate Did)");
			hm.put(210,"Le Psaume 23�/�Le Choix des armes�(The 23rd Psalm)");
			hm.put(211,"En territoire ennemi�/�Chasse � l'homme�(The Hunting Party)");
			hm.put(212,"Le Bapt�me�/�Le Sauveur�(Fire + Water)");
			hm.put(213,"Conflits�/�Manipulations�(The Long Con)");
			hm.put(214,"Un des leurs�/�L'Un d'entre eux�(One of Them)");
			hm.put(215,"Cong�s de maternit�/�Entre leurs mains�(Maternity Leave)");
			hm.put(216,"Toute la v�rit�(The Whole Truth)");
			hm.put(217,"Bloqu�!�/�Huis clos�(Lockdown)");
			hm.put(218,"Dans son monde�/�Dave�(Dave)");
			hm.put(219,"SOS�(S.O.S.)");
			hm.put(220,"Compagnon de d�route�(Two for the Road)");
			hm.put(221,"Sous surveillance�/�?�(?)");
			hm.put(222,"Ces quatre l�/�Trois minutes�(Three Minutes)");
			hm.put(223,"Vivre ensemble��/�Vivre ensemble, mourir seul, premi�re partie�(Live Together, Die Alone: Part One)");
			hm.put(224,"�Et mourir seul�/�Vivre ensemble, mourir seul, deuxi�me partie�(Live Together, Die Alone: Part Two)");
			hm.put(301,"De l'autre c�t�/�Un autre monde�(A Tale of Two Cities)");
			hm.put(302,"D'entre les morts�/�L'Art de la soumission�(The Glass Ballerina)");
			hm.put(303,"Embuscade�/�Le Chasseur�(Further Instructions)");
			hm.put(304,"Une histoire de c�ur�/�Chacun pour soi�(Every Man For Himself)");
			hm.put(305,"L'Heure du jugement�(The Cost of Living)");
			hm.put(306,"Coup d'�tat�/�Sacrifices�(I Do)");
			hm.put(307,"Loin de chez elle�/�Loin de Portland�(Not In Portland)");
			hm.put(308,"Impression de d�j� vu�(Flashes Before Your Eyes)");
			hm.put(309,"�tranger parmi eux�/�Chez eux�(Stranger In A Strange Land)");
			hm.put(310,"Chance et malchance�(Tricia Tanaka Is Dead)");
			hm.put(311,"Tapez 77�(Enter 77)");
			hm.put(312,"La Voie des airs�(Par Avion)");
			hm.put(313,"Sans retour�(The Man from Tallahassee)");
			hm.put(314,"Jusque dans la tombe�/�� l'amour, � la mort�(Expos�)");
			hm.put(315,"Meilleures Ennemies�/�� l'amende�(Left Behind)");
			hm.put(316,"Une des n�tres�/�Persona non grata�(One Of Us)");
			hm.put(317,"L'Effet papillon�/�Avis de d�c�s�(Catch 22)");
			hm.put(318,"Histoire de femmes�/�Un heureux �v�nement�(D.O.C.)");
			hm.put(319,"Mon p�re cet escroc�/�Filiations�(The Brig)");
			hm.put(320,"L'Homme de l'ombre�/�L'homme qui tire les ficelles�(The Man Behind the Curtain)");
			hm.put(321,"Meilleurs Moments�(Greatest Hits)");
			hm.put(322,"L� o� tout commence��/�L'Envers de l'oc�an, premi�re partie�(Through the Looking Glass: Part One)");
			hm.put(323,"�et tout finit.�/�L'Envers de l'oc�an, deuxi�me partie�(Through the Looking Glass: Part Two)");
			hm.put(401,"Le D�but de la fin�(The Beginning of the End)");
			hm.put(402,"Enfin les secours�?�/�La Voix des morts�(Confirmed Dead)");
			hm.put(403,"Liste noire�/�L'�conomiste�(The Economist)");
			hm.put(404,"M�res ennemies�/�Libert� surveill�e�(Eggtown)");
			hm.put(405,"Perdu dans le temps�(The Constant)");
			hm.put(406,"L'Autre femme�/�Jalousies�(The Other Woman)");
			hm.put(407,"Le Choix du pardon�/�Ji Yeon�(Ji Yeon)");
			hm.put(408,"Seconde chance�(Meet Kevin Johnson)");
			hm.put(409,"De nouvelles r�gles�(The Shape of Things to Come)");
			hm.put(410,"Une part de soi�/�D'heureuses perspectives�(Something Nice Back Home)");
			hm.put(411,"Le Messager�/�La Cabane�(Cabin Fever)");
			hm.put(412,"Ceux qui restent, premi�re partie�/�Les Six du vol Oc�anic, premi�re partie�(There's No Place Like Home: Part One)");
			hm.put(413,"Ceux qui restent, deuxi�me partie�/�Les Six du vol Oc�anic, deuxi�me partie�(There's No Place Like Home: Part Two)");
			hm.put(414,"Ceux qui restent, troisi�me partie�/�Les Six du vol Oc�anic, troisi�me partie�(There's No Place Like Home: Part Three)");
			hm.put(501,"Les Exil�s�/�Le jour o� l'�le s�est d�plac�e�(Because You Left)");
			hm.put(502,"Ne jamais mentir�/�Le Mensonge�(The Lie)");
			hm.put(503,"Bombe H�/�La Bombe�(Jughead)");
			hm.put(504,"Le Petit Prince�/�Pour Aaron�(The Little Prince)");
			hm.put(505,"Retour � l'Orchid�e�/�Voyage vers l'Orchid�e�(The Place Is Death)");
			hm.put(506,"Le Vol 316�(316)");
			hm.put(507,"La Vie et la Mort de Jeremy Bentham�/�La Vie de Jeremy Bentham�(The Life and Death of Jeremy Bentham)");
			hm.put(508,"Monsieur LaFleur�(LaFleur)");
			hm.put(509,"Namaste�(Namaste)");
			hm.put(510,"Le Prisonnier�/�Un nouvel ennemi�(He's Our You)");
			hm.put(511,"Le pass�, c'est le pass�/�Pass� recompos�(Whatever Happened, Happened)");
			hm.put(512,"Ben et l'enfant�/�Le Jugement�(Dead Is Dead)");
			hm.put(513,"Parle avec eux�/�Le Syndrome Skywalker�(Some Like It Hoth)");
			hm.put(514,"Le Facteur humain�/�La Variable�(The Variable)");
			hm.put(515,"Suivez le guide�/�L'�vacuation�(Follow the Leader)");
			hm.put(516,"Au bout du voyage, premi�re partie�/�L'Incident, premi�re partie�(The Incident: Part One)");
			hm.put(517,"Au bout du voyage, deuxi�me partie�/�L'Incident, deuxi�me partie�(The Incident: Part Two)");
			hm.put(601,"On efface tout��/�Los Angeles, premi�re partie�(LA X: Part One)");
			hm.put(602,"�Et on recommence�/�Los Angeles, deuxi�me partie�(LA X: Part Two)");
			hm.put(603,"Cours Kate, cours�/�La Proie des t�n�bres�(What Kate Does)");
			hm.put(604,"Le Rempla�ant�(The Substitute)");
			hm.put(605,"Jeu de miroirs�/�Le Phare�(Lighthouse)");
			hm.put(606,"La Marche des t�n�bres�/�� la nuit tomb�e�(Sundown)");
			hm.put(607,"Professeur Linus�(Dr. Linus)");
			hm.put(608,"L'�claireur�(Recon)");
			hm.put(609,"Depuis la nuit des temps�/�L'Immortel�(Ab Aeterno)");
			hm.put(610,"Mademoiselle Paik�/�Le Paquet�(The Package)");
			hm.put(611,"Et ils v�curent heureux�(Happily Ever After)");
			hm.put(612,"Tout le monde aime Hugo�/�Le bien-aim�(Everybody Loves Hugo)");
			hm.put(613,"La Derni�re Recrue�(The Last Recruit)");
			hm.put(614,"Rassembler pour mieux tuer�/�Le Candidat�(The Candidate)");
			hm.put(615,"� la source�(Across the Sea)");
			hm.put(616,"Au nom des disparus�(What They Died For)");
			hm.put(617,"Fin, premi�re partie�(The End: Part One)");
			hm.put(618,"Fin, deuxi�me partie�(The End: Part Two)");

			
			System.out.println("Parcours de l'objet HashMap : ");
		    Set<Entry<Integer, String>> setHm = hm.entrySet();
		    Iterator<Entry<Integer, String>> it = setHm.iterator();
		    while(it.hasNext()){
		    	Entry<Integer, String> e = it.next();
		    	int seasonIndex = (e.getKey()/100)-1;
		    	System.out.println("Ep : " + e.getKey()%100 + " : " + e.getValue() + " / season index : " + seasonIndex);
		    	Episode ep = new Episode();
		    	ep.setNumber( e.getKey()%100);
		    	ep.setTitle(e.getValue());
		    	
		    	lostSeasons.get(seasonIndex).addEpisode(ep);    //[(e.getKey()%100)].addEpisode(ep);
		    	
		    	em.persist(ep);
		        
		      }
			
			/*List<Season> sList = (List<Season>) em.createQuery("FROM Season s").getResultList();
			System.out.println("list size: " + sList.size());
			*/
			
		    User sam = (User) em.createQuery("FROM User u where u.username=:un").setParameter("un", "sc")
					.getSingleResult();

			List<Episode> episodes = (List<Episode>) em.createQuery("FROM Episode e").getResultList();
			System.out.println("list size: " + episodes.size() + " first ep : " + episodes.get(0).getTitle());

			for (int i = 0; i < episodes.size(); i++) {
				Library l = new Library();
				Episode ep = episodes.get(i);
				System.out.println(ep);
				if(i%2==0)
					l.setWatched(false);
				else
					l.setWatched(true);
				l.setUser(sam);
				l.setEpisode(ep);
				ep.addLibrary(l);
				sam.addLibrary(l);
				em.persist(l);
			}			
	
	}
}