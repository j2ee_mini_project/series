package ch.hevs.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import ch.hevs.businessobject.Episode;
import ch.hevs.businessobject.Library;
import ch.hevs.businessobject.Season;
import ch.hevs.businessobject.Serie;
import ch.hevs.businessobject.User;
import ch.hevs.serieservice.TvSerieBean;
import ch.hevs.serieservice.TvSeries;

public class Friends {
	private EntityManager em;

	public Friends(EntityManager em) {
		this.em = em;

		/*
		 * LOST
		 */
		// SERIE
		Serie friends = new Serie();
		friends.setName("Friends");
		em.persist(friends);

		// SEASONS
		int friendsSeasonYear[] = { 1994, 1995, 1996, 1997 };
		List<Season> friendsSeasons = new ArrayList<Season>();

		for (int i = 0; i < friendsSeasonYear.length; i++) {
			int SeasonNumber = i + 1;
			Season s = new Season();
			s.setNumber(SeasonNumber);
			s.setYear(friendsSeasonYear[i]);
			friendsSeasons.add(s);
			friends.addSeason(friendsSeasons.get(i));
			em.persist(s);
		}

		// EPISODES
		Map<Integer, String> hm = new LinkedHashMap<>();
		hm.put(101, "Celui qui d�m�nage�(The One Where Monica Gets A New Roommate)");
		hm.put(102, "Celui qui est perdu�(The One With the Sonogram at the End)");
		hm.put(103, "Celui qui a un r�le�(The One With the Thumb)");
		hm.put(104, "Celui avec George (The One With George Stephanopoulos)");
		hm.put(105, "Celui qui lave plus blanc�(The One With the East German Laundry Detergent)");
		hm.put(106, "Celui qui est verni�(The One With the Butt)");
		hm.put(107, "Celui qui a du jus�(The One With the Blackout)");
		hm.put(108, "Celui qui hallucine�(The One Where Nana Dies Twice)");
		hm.put(109, "Celui qui parle au ventre de sa femme�(The One Where Underdog Gets Away)");
		hm.put(110, "Celui qui singeait�(The One With the Monkey)");
		hm.put(111, "Celui qui �tait comme les autres�(The One With Mrs. Bing)");
		hm.put(112, "Celui qui aimait les lasagnes�(The One With the Dozen Lasagnas)");
		hm.put(113, "Celui qui fait des descentes dans les douches�(The One With the Boobies)");
		hm.put(114, "Celui qui avait un c�ur d'artichaut�(The One With the Candy Hearts)");
		hm.put(115, "Celui qui p�te les plombs�(The One With the Stoned Guy)");
		hm.put(116, "Celui qui devient papa ��1re�partie�(The One With Two Parts � Part 1)");
		hm.put(117, "Celui qui devient papa ��2e�partie�(The One With Two Parts � Part 2)");
		hm.put(118, "Celui qui gagnait au poker�(The One With All The Poker)");
		hm.put(119, "Celui qui a perdu son singe�(The One Where the Monkey Gets Away)");
		hm.put(120, "Celui qui a un dentiste cari�(The One With the Evil Orthodontist)");
		hm.put(121, "Celui qui avait un singe�(The One With the Fake Monica)");
		hm.put(122, "Celui qui r�ve par procuration�(The One With the Ick Factor)");
		hm.put(123, "Celui qui a failli rater l'accouchement�(The One With the Birth)");
		hm.put(124, "Celui qui fait craquer Rachel�(The One Where Rachel Finds Out)");
		hm.put(201, "Celui qui a une nouvelle fianc�e�(The One With Ross's New Girlfriend)");
		hm.put(202, "Celui qui d�testait le lait maternel�(The One With The Breast Milk)");
		hm.put(203, "Celui qui est mort dans l'appart du dessous�(The One Where Heckles Dies)");
		hm.put(204, "Celui qui avait vir� de bord�(The One With Phoebe's Husband)");
		hm.put(205, "Celui qui se faisait passer pour Bob�(The One With Five Steaks And An Eggplant)");
		hm.put(206, "Celui qui a oubli� un b�b� dans le bus�(The One With The Baby On The Bus)");
		hm.put(207, "Celui qui tombe des nues�(The One Where Ross Finds Out)");
		hm.put(208, "Celui qui a �t� tr�s maladroit�(The One With The List)");
		hm.put(209, "Celui qui cassait les radiateurs�(The One With Phoebe's Dad)");
		hm.put(210, "Celui qui se d�double�(The One With Russ)");
		hm.put(211, "Celui qui n'appr�cie pas certains mariages�(The One with the Lesbian Wedding)");
		hm.put(212, "Celui qui retrouve son singe -�1re�partie�(The One After The Super Bowl - Part 1)");
		hm.put(213, "Celui qui retrouve son singe -�2e�partie�(The One After The Super Bowl - Part 2)");
		hm.put(214, "Celui qui a failli aller au bal de promo�(The One With The Prom Video)");
		hm.put(215, "Celui qui a fait on ne sait quoi avec Rachel�(The One Where Ross and Rachel... You Know)");
		hm.put(216, "Celui qui vit sa vie�(The One Where Joey Moves Out)");
		hm.put(217, "Celui qui remplace celui qui part�(The One Where Eddie Moves In)");
		hm.put(218, "Celui qui dispara�t de la s�rie�(The One Where Dr. Ramoray Dies)");
		hm.put(219, "Celui qui ne voulait pas partir�(The One Where Eddie Won't Go)");
		hm.put(220, "Celui qui se met � parler�(The One Where Old Yeller Dies)");
		hm.put(221, "Celui qui affronte les voyous�(The One With The Bullies)");
		hm.put(222, "Celui qui faisait le lien�(The One With The Two Parties)");
		hm.put(223, "Celui qui attrape la varicelle�(The One With The Chicken Pox)");
		hm.put(224, "Celui qui embrassait mal�(The One With Barry And Mindy's Wedding)");
		hm.put(301, "Celui qui r�vait de la princesse Leia�(The One With The Princess Leia Fantasy)");
		hm.put(302, "Celui qui a du mal � se pr�parer�(The One Where No One's Ready)");
		hm.put(303, "Celui qui avait la technique du c�lin�(The One With The Jam)");
		hm.put(304, "Celui qui ne supportait pas les poup�es�(The One With The Metaphorical Tunnel)");
		hm.put(305, "Celui qui bricolait�(The One With Frank Jr.)");
		hm.put(306, "Celui qui se souvient�(The One With The Flashback)");
		hm.put(307, "Celui qui �tait prof et �l�ve�(The One With The Race Car Bed)");
		hm.put(308, "Celui qui avait pris un coup sur la t�te�(The One With The Giant Poking Device)");
		hm.put(309, "Celui pour qui le foot c'est pas le pied�(The One With The Football)");
		hm.put(310, "Celui qui fait d�missionner Rachel�(The One Where Rachel Quits)");
		hm.put(311, "Celui qui ne s'y retrouvait plus�(The One Where Chandler Can't Remember Which Sister)");
		hm.put(312, "Celui qui �tait tr�s jaloux�(The One With All The Jealousy)");
		hm.put(313, "Celui qui persiste et signe�(The One Where Monica And Richard Are Just Friends)");
		hm.put(314, "Celui que les proth�ses ne g�naient pas�(The One With Phoebe's Ex-Partner)");
		hm.put(315, "Celui qui vivait mal la rupture�(The One Where Ross And Rachel Take A Break)");
		hm.put(316, "Celui qui a surv�cu au lendemain�(The One The Morning After)");
		hm.put(317, "Celui qui �tait laiss� pour compte�(The One Without The Ski Trip)");
		hm.put(318, "Celui qui s'auto-hypnotisait�(The One With The Hypnosis Tape)");
		hm.put(319, "Celui qui avait un tee-shirt trop petit�(The One With The Tiny T-Shirt)");
		hm.put(320, "Celui qui courait deux li�vres�(The One With The Dollhouse)");
		hm.put(321, "Celui qui avait un poussin�(The One With A Chick And A Duck)");
		hm.put(322, "Celui qui s'�nervait�(The One With The Screamer)");
		hm.put(323, "Celui qui avait un truc dans le dos�(The One With Ross's Thing)");
		hm.put(324, "Celui qui voulait �tre ultime champion�(The One With The Ultimate Fighting Champion)");
		hm.put(325, "Celui qui allait � la plage�(The One At The Beach)");
		hm.put(401, "Celui qui soignait les piq�res de m�duses�(The One With The Jellyfish)");
		hm.put(402, "Celui qui ne voyait qu'un chat�(The One With The Cat)");
		hm.put(403, "Celui qui avait des menottes�(The One With The Cuffs)");
		hm.put(404, "Celui qui apprenait � danser�(The One With The Ballroom Dancing)");
		hm.put(405, "Celui qui avait une nouvelle copine�(The One With Joey's New Girlfriend)");
		hm.put(406, "Celui qui fr�quentait une souillon�(The One With The Dirty Girl)");
		hm.put(407, "Celui qui poussait le bouchon�(The One Where Chandler Crosses The Line)");
		hm.put(408, "Celui qui �tait dans la caisse�(The One With Chandler In A Box)");
		hm.put(409, "Celui qui savait faire la f�te�(The One Where They're Going To Party!)");
		hm.put(410, "Celui qui draguait au large�(The One With The Girl From Poughkeepsie)");
		hm.put(411, "Celui qui posait une question embarrassante�(The One With Phoebe's Uterus)");
		hm.put(412, "Celui qui gagnait les paris�(The One With The Embryos)");
		hm.put(413, "Celui qui se gourait du tout au tout�(The One With Rachel's Crush)");
		hm.put(414, "Celui qui n'avait pas le moral�(The One With Joey's Dirty Day)");
		hm.put(415, "Celui qui jouait au rugby�(The One With All The Rugby)");
		hm.put(416, "Celui qui participait � une f�te bidon�(The One With The Fake Party)");
		hm.put(417, "Celui qui avait la cha�ne porno�(The One With The Free Porn)");
		hm.put(418, "Celui qui cherche un pr�nom�(The One With Rachel's New Dress)");
		hm.put(419, "Celui qui faisait de grands projets�(The One With All The Haste)");
		hm.put(420, "Celui qui va se marier�(The One With The Wedding Dresses)");
		hm.put(421, "Celui qui envoie l'invitation�(The One With The Invitations)");
		hm.put(422, "Celui qui �tait le pire t�moin du monde�(The One With The Worst Best Man Ever)");
		hm.put(423, "Celui qui se marie (partie 1)�(The One With Ross's Wedding - Part 1)");
		hm.put(424, "Celui qui se marie (partie 2)�(The One With Ross's Wedding - Part 2)");

		System.out.println("Parcours de l'objet HashMap : ");
		Set<Entry<Integer, String>> setHm = hm.entrySet();
		Iterator<Entry<Integer, String>> it = setHm.iterator();
		while (it.hasNext()) {
			Entry<Integer, String> e = it.next();
			int seasonIndex = (e.getKey() / 100) - 1;
			System.out.println("Ep : " + e.getKey() % 100 + " : " + e.getValue() + " / season index : " + seasonIndex);
			Episode ep = new Episode();
			ep.setNumber(e.getKey() % 100);
			ep.setTitle(e.getValue());

			friendsSeasons.get(seasonIndex).addEpisode(ep); // [(e.getKey()%100)].addEpisode(ep);

			em.persist(ep);

		}

		/*
		 * List<Season> sList = (List<Season>)
		 * em.createQuery("FROM Season s").getResultList();
		 * System.out.println("list size: " + sList.size());
		 */

		User jack = (User) em.createQuery("FROM User u where u.username=:un").setParameter("un", "jh")
				.getSingleResult();

		List<Episode> episodes = (List<Episode>) em.createQuery("FROM Episode e").getResultList();
		System.out.println("list size: " + episodes.size() + " first ep : " + episodes.get(0).getTitle());

		for (int i = 0; i < episodes.size(); i++) {
			Library l = new Library();
			Episode ep = episodes.get(i);
			System.out.println(ep);
			if(i%2==0)
				l.setWatched(false);
			else
				l.setWatched(true);
			l.setUser(jack);
			l.setEpisode(ep);
			ep.addLibrary(l);
			jack.addLibrary(l);
			em.persist(l);
		}

	}
}